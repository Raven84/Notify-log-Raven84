#!/bin/sh

tail -n0 -f /var/log/kern.log | \
grep --line-buffered "Manufacturer" | \

while read LINE; do 

time=$(echo "$LINE" | awk '{print $1" "$2" "$3}')
manufacturer=$(echo "$LINE" | awk '{print $10}')
notify-send "Hardware" "$time $manufacturer" -h string:bgcolor:#3B6B29

mkdir -p /mnt/$manufacturer

done

