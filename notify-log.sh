#!/bin/sh
iptables -A INPUT -j LOG --log-prefix "Intrusion ->"
iptables -A FORWARD -j LOG --log-prefix "Intrusion ->"

tail -n0 -f /var/log/kern.log | \
	grep --line-buffered "Intrusion" | \

while read LINE; do 

intrusion=$(echo "$LINE" | awk '{print $1" "$2" "$3" "$8" "$11" "$12" "$13" "$16" "$19" "$20" "$21}')
notify-send "Firewall Alert \!" "$intrusion" -h string:bgcolor:#AB2C2C

done
