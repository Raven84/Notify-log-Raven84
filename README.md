### Install :

```bash
apt-get install dunst libnotify-bin
git clone https://framagit.org/Raven84/Notify-log-Raven84.git
chmod 755 notify-log.sh
./notify-log.sh & 		#Background
```
###### Notify with dunst and notify-send
```bash
mv dunstrc ~/.config/dunst/
```

