#!/bin/bash
# cp notify-coffee.sh /etc/cron.hourly/
# echo " 0 *    * * *   $USER    /etc/cron.hourly/notify-coffee.sh" >> /etc/crontab
export DISPLAY=:0.0
export XAUTHORITY=/home/$USER/.Xauthority
date=$(date '+%Hh%Mm%Ss')
notify-send "☕ Coffee time ! $date" "☕" -h string:bgcolor:#604839

